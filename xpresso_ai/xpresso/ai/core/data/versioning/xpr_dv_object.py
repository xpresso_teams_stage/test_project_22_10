
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.exceptions.xpr_exceptions import *
from copy import deepcopy


class XprDataVersioningObject(object):
    """
    This class serves as the superclass of all objects managed by the controller
    e.g., User, Node, Cluster, etc.
    """

    def __init__(self, obj_json=None):
        self.logger = XprLogger()
        self.logger.debug("Inside XprDataVersioningObject constructor")
        # Initialising the data with deepcopy of obj_json is better way
        self.data = deepcopy(obj_json)
        self.logger.debug("Done")

        self.mandatory_fields = None
        self.valid_values = None
        self.unmodifiable_fields = None
        self.valid_field_types = None
        self.conditional_mandatory_fields = ()

    def set(self, key, value):
        self.data[key] = value

    def get(self, key, default_value=None):
        return self.data.get(key, default_value)

    def validate_mandatory_fields(self):
        self.logger.debug("Validating mandatory fields")
        """
        checks if the mandatory fields for an object have been specified
        """
        for f in self.mandatory_fields:
            self.logger.debug("Validating field {}".format(f))
            if f not in self.data:
                raise MissingFieldException(f'Field "{f}" is missing in '
                                            'the input.')
            elif not len(self.data[f]):
                raise BlankFieldException(
                    f'Field "{f}" cannot be blank in the input.')

    def validate_field_values(self):
        """
        checks if the value of the specified field is valid
        """
        for field in self.valid_values:
            if field in self.data and \
               self.data[field] not in self.valid_values[field]:
                raise InvalidValueException(
                    f'Value "{self.data[field]}" is invalid for the'
                    f' input field "{field}".')

    def validate_modifiable_fields(self):
        self.logger.debug("Validating modifiable fields")
        """
        checks if the mandatory fields for an object have been specified
        """
        for f in self.unmodifiable_fields:
            self.logger.debug("Validating field {}".format(f))
            if f in self.data:
                raise IllegalModificationException(
                    f'Field "{f}" cannot be modified')

    def validate_field_types(self):
        """
        validates all the XprObject field's data-types
        """
        # valid_field_types is a dictionary with mapping of
        # field_names to their valid data type
        for field, field_type in self.valid_field_types.items():
            if field in self.data and self.data[field]:
                if not isinstance(self.data[field], field_type):
                    raise FieldTypeException(field, str(field_type))

    def validate_conditional_mandatory_fields(self):
        """
        Validates if mandatory fields for experiment object are provided
         with a condition
        """
        field_present_flag = False
        if not len(self.conditional_mandatory_fields):
            return
        valid_list = list()
        for field_list in self.conditional_mandatory_fields:
            if set(field_list).issubset(set(self.data)):
                field_present_flag = True
                valid_list = deepcopy(field_list)
                break

        if not field_present_flag:
            raise MissingFieldException(
                "Incomplete input to create this object"
            )

        for field in valid_list:
            if not self.data[field]:
                raise BlankFieldException(
                    f"Invalid input. Mandatory field `{field}` is empty."
                )
            
    def validate_field(self, key: str):
        """
        Validates a field of the XprObject.
        This includes the type check, empty check value

        Args:
            key: name of the field that needs to be validated
        """
        if key not in self.data:
            # If key is not available in data
            raise MissingFieldException(f'Field "{key}" is missing in '
                                        'the input.')
        elif not len(self.data[key]):
            raise BlankFieldException(
                f'Field "{key}" cannot be blank in the input.')
        # check if valid_field_types is provided for the object
        if self.valid_field_types is not None and len(self.valid_field_types):
            if key not in self.valid_field_types:
                # If provided key is not specified in valid_field_type,
                # then it is not valid
                raise XprFieldException(key)
            if not isinstance(self.data[key], self.valid_field_types[key]):
                raise FieldTypeException(key, str(self.valid_field_types[key]))
        return True
